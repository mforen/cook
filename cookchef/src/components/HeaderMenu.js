import styles from './HeaderMenu.Module.scss';
function HeaderMenu() {
    return (
        <ul className={`${styles.MenuContainer} card p-20`}>
            <li>Wishlist</li>
            <li>Connexion</li>
        </ul>
    );
}

export default HeaderMenu;